package com.example.android.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scorePlayerOne = 0;
    int scorePlayerTwo = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    //adding points for player one
    public void addSevenForPlayerOne(View view) {
        displayForPlayerOne(scorePlayerOne += 7);
    }
    public void addSixForPlayerOne(View view) {
        displayForPlayerOne(scorePlayerOne += 6);
    }
    public void addFiveForPlayerOne(View view) {
        displayForPlayerOne(scorePlayerOne += 5);
    }
    public void addFourForPlayerOne(View view) {
        displayForPlayerOne(scorePlayerOne += 4);
    }
    public void addThreeForPlayerOne(View view) {
        displayForPlayerOne(scorePlayerOne += 3);
    }
    public void addTwoForPlayerOne (View view) {
        displayForPlayerOne(scorePlayerOne += 2);
    }
    public void addOneForPlayerOne (View view) {
        displayForPlayerOne(scorePlayerOne += 1);
    }
    //adding points for player two
    public void addSevenForPlayerTwo(View view) {
        displayForPlayerTwo(scorePlayerTwo += 7);
    }
    public void addSixForPlayerTwo(View view) {
        displayForPlayerTwo(scorePlayerTwo += 6);
    }
    public void addFiveForPlayerTwo(View view) {
        displayForPlayerTwo(scorePlayerTwo += 5);
    }
    public void addFourForPlayerTwo(View view) {
        displayForPlayerTwo(scorePlayerTwo += 4);
    }
    public void addThreeForPlayerTwo(View view) {
        displayForPlayerTwo(scorePlayerTwo += 3);
    }
    public void addTwoForPlayerTwo (View view) {
        displayForPlayerTwo(scorePlayerTwo += 2);
    }
    public void addOneForPlayerTwo (View view) {
        displayForPlayerTwo(scorePlayerTwo += 1);
    }

    public void displayForPlayerOne(int score) {
        TextView scoreView = (TextView) findViewById(R.id.player_One_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayForPlayerTwo(int score) {
        TextView scoreView = (TextView) findViewById(R.id.player_Two_score);
        scoreView.setText(String.valueOf(score));
    }

    public void resetAll (View view) {
        scorePlayerOne = 0;
        scorePlayerTwo =0;
        displayForPlayerOne(scorePlayerOne);
        displayForPlayerTwo(scorePlayerTwo);
    }

}
